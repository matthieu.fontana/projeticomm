<?php

namespace App\Controller;

use App\Entity\Sondage;
use App\Entity\Reponse;
use App\Entity\Question;
use App\Entity\Feedback;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\Material\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\TreeMapChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\OrgChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ComboChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Options\ComboChart\Series;
use Symfony\Component\HttpFoundation\Request;

class SondageController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $sondage = $this->getDoctrine()
            ->getRepository(Sondage::class)
            ->findAll();
        return $this->render('graph/index.html.twig', [
            'sondage' => $sondage
        ]);
        
    }

    /**
     * @Route("/feedback/{id}", name="feedback", methods={"POST"})
     */
    public function feedback(Feedback $feedback): Response
    {
      return $this->render('sondage/feedback.html.twig', [
        'feedback' => $feedback]);
    }


    
    /**
     * @Route("/graph1", name="graph1")
     */
    public function columnChart($data, $questionName)
    {
        $chart = new ColumnChart();
      $chart->getData()->setArrayToDataTable($data);
      $chart->getOptions()->getChart()->setTitle($questionName);
      $chart->getOptions()
          ->setBars('vertical')
          ->setHeight(300)
          ->setWidth(700)
          ->setColors(['#34c924'])
          ->getVAxis()
          ->setFormat('decimal');
        return $chart;
 
    
    }

    /**
     * @Route("/graph2", name="graph2")
     */
    public function graph2()
    {
        $org = new OrgChart();
        $org->getData()->setArrayToDataTable(
            [
                [['v' => 'Mike', 'f' => 'Mike<div style="color:red; font-style:italic">President</div>'], '', 'The President'],
                [['v' => 'Jim', 'f' => 'Jim<div style="color:red; font-style:italic">Vice President</div>'], 'Mike', 'VP'],
                ['Alice', 'Mike', ''],
                ['Bob', 'Jim', 'Bob Sponge'],
                ['Carol', 'Bob', '']
            ],
            true
        );
        $org->getOptions()->setAllowHtml(true);

        return $this->render('graph/graph.html.twig', [
            'title' => 'Org Chart',
            'chart' => $org
        ]);
    }

    /**
     * @Route("/graph3", name="graph3")
     */
    public function graph3()
    {
        $combo = new ComboChart();
        $combo->getData()->setArrayToDataTable([
            ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
            ['2004/05',  165,      938,         522,             998,           450,      614.6],
            ['2005/06',  135,      1120,        599,             1268,          288,      682],
            ['2006/07',  157,      1167,        587,             807,           397,      623],
            ['2007/08',  139,      1110,        615,             968,           215,      609.4],
            ['2008/09',  136,      691,         629,             1026,          366,      569.6]
        ]);
        $combo->getOptions()->setTitle('Monthly Coffee Production by Country');
        $combo->getOptions()->getVAxis()->setTitle('Cups');
        $combo->getOptions()->getHAxis()->setTitle('Month');
        $combo->getOptions()->setSeriesType('bars');

        $series5 = new Series();
        $series5->setType('line');
        $combo->getOptions()->setSeries([5 => $series5]);

        $combo->getOptions()->setWidth(900);
        $combo->getOptions()->setHeight(500);

        return $this->render('graph/graph.html.twig', [
            'title' => 'Combo',
            'chart' => $combo
        ]);
    }
}
